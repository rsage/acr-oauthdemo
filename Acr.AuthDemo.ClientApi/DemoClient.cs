﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Security.Authentication;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Acr.AuthDemo.Common;
using Newtonsoft.Json;
using Thinktecture.IdentityModel.Client;

namespace Acr.AuthDemo.ClientApi
{
    public class DemoClient: IDisposable
    {
        #region Fields

        private readonly string _facilityId;

        private readonly Uri _getTokenUrl;
        private readonly Uri _baseTrialUrl;
        private readonly Uri _baseFilesUrl;
        private TokenResponse _authResponse;

        private readonly HttpClient _httpClient = new HttpClient();

        #endregion Fields

        #region Constructors

        public DemoClient(Uri baseApiUrl, string facilityId)
        {
            _facilityId = facilityId;

            var urlBuilder = new UriBuilder(baseApiUrl);

            urlBuilder.Path = "token";
            _getTokenUrl = urlBuilder.Uri;

            urlBuilder.Path = "api/trial";
            _baseTrialUrl = urlBuilder.Uri;

            urlBuilder.Path = "api/files";
            _baseFilesUrl = urlBuilder.Uri;
        }

        #endregion Constructors

        #region Public Methods

        #region Auth

        public async Task AuthenticateAsync(string userName, string password)
        {
            var authResponse = await OAuthClient.RequestResourceOwnerPasswordAsync(userName, password, "nuance");

            ValidateAuthError(authResponse);

            _authResponse = authResponse;
        }

        public async Task RefreshTokenAsync(string refreshToken)
        {
            var authResponse = await OAuthClient.RequestRefreshTokenAsync(refreshToken);

            ValidateAuthError(authResponse);

            _authResponse = authResponse;
        }

        public string RefreshToken
        {
            get
            {
                ValidateAuthorized();
                return _authResponse.RefreshToken;
            }
        }

        #endregion Auth

        #region API

        public async Task<IEnumerable<Trial>> GetAllTrialsAsync()
        {
            await RefreshTokenIfRequiredAsync();

            var result = await _httpClient.GetAsync(_baseTrialUrl).ConfigureAwait(false);

            result.EnsureSuccessStatusCode();

            var rawData = await result.Content.ReadAsStringAsync().ConfigureAwait(false);

            return await Task.Factory.StartNew(()=>JsonConvert.DeserializeObject<Trial[]>(rawData)).ConfigureAwait(false);
        }

        public async Task<Trial> GetTrialAsync(string id)
        {
            await RefreshTokenIfRequiredAsync();

            var urlBuilder = new UriBuilder(_baseTrialUrl);
            urlBuilder.Path = urlBuilder.Path +"/" + id;

            var result = await _httpClient.GetAsync(urlBuilder.Uri).ConfigureAwait(false);

            result.EnsureSuccessStatusCode();

            var rawData = await result.Content.ReadAsStringAsync().ConfigureAwait(false);

            return await Task.Factory.StartNew(() => JsonConvert.DeserializeObject<Trial>(rawData)).ConfigureAwait(false);
        }

        public async Task AddTrialAsync(Trial newTrial)
        {
            await RefreshTokenIfRequiredAsync();

            var serializedTrial = await Task.Factory.StartNew(()=>JsonConvert.SerializeObject(newTrial)).ConfigureAwait(false);
            HttpContent content = new StringContent(serializedTrial, Encoding.UTF8, "application/json");
            var result = await _httpClient.PostAsync(_baseTrialUrl, content).ConfigureAwait(false);

            result.EnsureSuccessStatusCode();
        }

        public async Task UpdateTrialAsync(Trial trial)
        {
            await RefreshTokenIfRequiredAsync();

            var serializedTrial = await Task.Factory.StartNew(() => JsonConvert.SerializeObject(trial)).ConfigureAwait(false);
            HttpContent content = new StringContent(serializedTrial, Encoding.UTF8, "application/json");
            var result = await _httpClient.PutAsync(_baseTrialUrl, content).ConfigureAwait(false);

            result.EnsureSuccessStatusCode();
        }

        public async Task DeleteTrialAsync(string id)
        {
            await RefreshTokenIfRequiredAsync();

            var urlBuilder = new UriBuilder(_baseTrialUrl);
            urlBuilder.Path = urlBuilder.Path + "/" + id;

            var result = await _httpClient.DeleteAsync(urlBuilder.Uri).ConfigureAwait(false);
            var data = await result.Content.ReadAsStringAsync();

            result.EnsureSuccessStatusCode();
        }

        public async Task<Stream> GetFileAsync(string fileName)
        {
            await RefreshTokenIfRequiredAsync();

            var urlBuilder = new UriBuilder(_baseFilesUrl);
            urlBuilder.Path = urlBuilder.Path + "/" + fileName;

            return await _httpClient.GetStreamAsync(urlBuilder.Uri);
        }

        public async Task UploadFileAsync(Stream file, string fileName)
        {
            await RefreshTokenIfRequiredAsync();

            HttpContent content = new StreamContent(file);

            var urlBuilder = new UriBuilder(_baseFilesUrl);
            
            urlBuilder.Path = urlBuilder.Path + "/" + HttpUtility.UrlEncode(fileName);

            var result = await _httpClient.PostAsync(urlBuilder.Uri, content);

            result.EnsureSuccessStatusCode();
        }

        #endregion API

        #endregion Public Methods

        #region Helpers
        private OAuth2Client OAuthClient
        {
            get { return new OAuth2Client(_getTokenUrl, _facilityId, null, OAuth2Client.ClientAuthenticationStyle.PostValues); }
        }

        private async Task RefreshTokenIfRequiredAsync()
        {
            ValidateAuthorized();

            if (_authResponse.Json.Value<DateTime>(".expires") <= DateTime.Now)
            {
                await RefreshTokenAsync(_authResponse.RefreshToken);
            }

            _httpClient.SetBearerToken(_authResponse.AccessToken);
        }

        private void ValidateAuthorized()
        {
            if (_authResponse == null)
            {
                throw new InvalidOperationException("Authentication information is absent. Authenticate before any other operation perfoming.");
            }
        }

        private static void ValidateAuthError(TokenResponse authResponse)
        {
            if (authResponse.IsError)
            {
                throw new AuthenticationException(authResponse.Error);
            }
        }

        #endregion Helpers

        #region IDisposable implementation

        public void Dispose()
        {
            if (_httpClient != null)
            {
                _httpClient.Dispose();
            }
        }

        #endregion IDisposable implementation
    }
}