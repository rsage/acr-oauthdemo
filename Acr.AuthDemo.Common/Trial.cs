﻿using Newtonsoft.Json;

namespace Acr.AuthDemo.Common
{
    public class Trial
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public Project[] Projects { get; set; }

        public override string ToString()
        {
            return string.Format("{0}#{1}", Id, Name);
        }
    }
}
