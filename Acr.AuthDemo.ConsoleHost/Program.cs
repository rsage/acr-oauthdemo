﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Acr.AuthDemo.Core;
using Microsoft.Owin.Hosting;

namespace Acr.AuthDemo.ConsoleHost
{
    static class Program
    {
        private static readonly Uri DefaultBaseUrl = new Uri("http://localhost:8888");

        static void Main(string[] args)
        {
            try
            {
                var baseAddress = GetBaseAddress(args);

                using (WebApp.Start<Startup>(baseAddress.AbsoluteUri))
                {
                    Trace.TraceInformation("Web API host has started. Base address: {0}", baseAddress);
                    Console.Write("Press 'Enter' to exit: ");
                    Console.ReadLine();
                }
            }
            catch (Exception e)
            {
                Trace.TraceError(e.ToString());
            }
        }

        private static Uri GetBaseAddress(IList<string> args)
        {
            Uri baseAddress;
            if (args.Count > 0 && Uri.TryCreate(args[0], UriKind.Absolute, out baseAddress))
            {
                return baseAddress;
            }
            Console.WriteLine("Application agruments contains no valid URL value. Default value will be used.");
            return DefaultBaseUrl;
        }
    }
}
