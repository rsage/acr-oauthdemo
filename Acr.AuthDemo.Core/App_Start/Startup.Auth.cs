﻿using System;
using Acr.AuthDemo.Core.Controllers;
using Acr.AuthDemo.Core.OAuth;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;

namespace Acr.AuthDemo.Core
{
    public partial class Startup
    {
        private static void ConfigureAuth(IAppBuilder app)
        {
            var options = new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/token"),
                Provider = new OAuthFlowProvider(),
                RefreshTokenProvider = new AccessTokenProvider(),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(Settings.AccessTokenExpiresIn),
                AllowInsecureHttp = true
            };

            app.UseOAuthBearerTokens(options);
        }
    }
}