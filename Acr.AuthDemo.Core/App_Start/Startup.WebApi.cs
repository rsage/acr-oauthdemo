﻿using System.Web.Http;
using Owin;

namespace Acr.AuthDemo.Core
{
    public partial class Startup
    {
        private static void ConfigureWebApi(IAppBuilder app)
        {
            var config = new HttpConfiguration();
            config.Routes.MapHttpRoute("DefaultApi", "api/{controller}/{id}", new {id = RouteParameter.Optional});

            app.UseWebApi(config);
        }
    }
}