using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;

namespace Acr.AuthDemo.Core.Controllers
{
    [LogException]
    [Authorize]
    public class FilesController : ApiController
    {
        public HttpResponseMessage Get(string id)
        {
            Trace.TraceInformation("GET /files/{0}", id);

            Trace.TraceInformation("Root files folder configured: {0}", Settings.FilesFolder);

            var filePath = Path.Combine(Settings.FilesFolder, id);
            Trace.TraceInformation("File path: {0}", filePath);

            if (!File.Exists(filePath))
            {
                Trace.TraceError("File {0} doesn't exists.", filePath);
                throw new IOException(string.Format("File {0} doesn't exists.", filePath));
            }

            var stream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read);

            var result = new HttpResponseMessage(HttpStatusCode.OK) {Content = new StreamContent(stream)};
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            return result;
        }

        public async Task<HttpResponseMessage> Post(string id)
        {
            Trace.TraceInformation("POST /files/{0}", id);

            Trace.TraceInformation("Root files folder configured: {0}", Settings.FilesFolder);

            using (var requestStream = await Request.Content.ReadAsStreamAsync())
            {
                try
                {
                    var filePath = Path.Combine(Settings.FilesFolder, id);
                    Trace.TraceInformation("File path: {0}", filePath);

                    using (Stream fileStream = File.Create(filePath))
                    {
                        await requestStream.CopyToAsync(fileStream);
                    }
                }
                catch (IOException e)
                {
                    Trace.TraceError(e.ToString());
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);
                }
            }

            return new HttpResponseMessage {StatusCode = HttpStatusCode.Created};
        }
    }
}