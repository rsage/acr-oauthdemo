﻿using System.Diagnostics;
using System.Web.Http.Filters;

namespace Acr.AuthDemo.Core.Controllers
{
    public class LogExceptionAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            Trace.TraceError(context.Exception.ToString());
        }
    }
}