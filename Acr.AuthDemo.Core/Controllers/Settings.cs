using System.Configuration;
using System.Globalization;

namespace Acr.AuthDemo.Core.Controllers
{
    public static class Settings
    {
        public static string FilesFolder
        {
            get { return ConfigurationManager.AppSettings["FilesFolder"] ?? ".\\files"; }
        }

        public static double AccessTokenExpiresIn
        {
            get
            {
                var strValue = ConfigurationManager.AppSettings["OAuthAccessTokenExpiresInMinutes"];
                double val;
                return double.TryParse(strValue, NumberStyles.Float, CultureInfo.CurrentCulture, out val) ? val : 1d;
            }
        }
    }
}