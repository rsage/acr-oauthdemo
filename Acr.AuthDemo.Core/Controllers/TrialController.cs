﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Web.Http;
using System.Collections.Concurrent;
using Acr.AuthDemo.Common;

namespace Acr.AuthDemo.Core.Controllers
{
    [LogException]
    [Authorize]
    public class TrialController : ApiController
    {
        private static readonly ConcurrentDictionary<string, Trial> Trials = new ConcurrentDictionary<string, Trial>();

        static TrialController()
        {
            Trials.TryAdd("1", new Trial {Id = "1", Name = "Trial1", Projects = new[] {new Project {Name = "Project1"}}});
            Trials.TryAdd("2", new Trial {Id = "2", Name = "Trial2", Projects = new[] {new Project {Name = "Project2"}}});
            Trials.TryAdd("3", new Trial {Id = "3", Name = "Trial3", Projects = new[] {new Project {Name = "Project3"}}});
        }

        public IEnumerable<Trial> Get()
        {
            Trace.TraceInformation("GET /trial/");
            return Trials.Values;
        }

        public Trial Get(string id)
        {
            Trace.TraceInformation("GET /trial/{0}", id);

            Trial trial;
            if (!Trials.TryGetValue(id, out trial))
            {
                Trace.TraceError("No trial '{0}' was found.", id);
                throw new InvalidOperationException(
                    string.Format("Cannot get trial '{0}'. The trial doesn't exists.", id));
            }
            return trial;
        }

        public void Post([FromBody]Trial trial)
        {
            Trace.TraceInformation("POST /trial/, trial={0}", trial);

            if (!Trials.TryAdd(trial.Id, trial))
            {
                Trace.TraceError("Trial '{0}' already exists.", trial.Id);
                throw new InvalidOperationException(
                    string.Format("Cannot add new trial '{0}:{1}'. The trial already exists.", trial.Id, trial.Name));
            }
        }

        public Trial Put([FromBody]Trial trial)
        {
            Trace.TraceInformation("PUT /trial/, trial={0}", trial);

            Trial originalTrial;
            if (!Trials.TryGetValue(trial.Id, out originalTrial))
            {
                Trace.TraceError("Trial '{0}' doesn't exist.", trial.Id);
                throw new InvalidOperationException(
                    string.Format("Cannot update trial '{0}'. The trial doesn't exists.", trial.Id));
            }

            if (!Trials.TryUpdate(trial.Id, trial, originalTrial))
            {
                Trace.TraceError("Trial '{0}' update conflict.", trial.Id);
                throw new InvalidOperationException(
                    string.Format("Cannot update trial '{0}'. Update conflict.", trial.Id));
            }

            return originalTrial;
        }

        [HttpDelete]
        public void Delete(string id)
        {
            Trace.TraceInformation("DELETE /trial/{0}", id);

            Trial removedTrial;
            if (!Trials.TryRemove(id, out removedTrial))
            {
                Trace.TraceError("Trial '{0}' doesn't exist.", id);
                throw new InvalidOperationException(
                    string.Format("Cannot remove trial '{0}'. The trial doesn't exist.", id));

            }
        }
    }
}
