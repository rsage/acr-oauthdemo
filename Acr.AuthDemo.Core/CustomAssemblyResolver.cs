﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Http.Dispatcher;

namespace Acr.AuthDemo.Core
{
    public class CustomAssemblyResolver : IAssembliesResolver
    {
        public ICollection<Assembly> GetAssemblies()
        {
            var currentAsembly = GetType().Assembly;
            var baseAssemblies = AppDomain.CurrentDomain.GetAssemblies().ToList();
            if (!baseAssemblies.Contains(currentAsembly))
            {
                baseAssemblies.Add(currentAsembly);
            }
            return baseAssemblies;
        }
    }
}