﻿namespace Acr.AuthDemo.Core.Data
{
    public class User
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string FacilityId { get; set; }
    }
}
