﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Acr.AuthDemo.Core.Data
{
    public class UsersRepository
    {
        public async Task<User> FindUserAsync(string userName)
        {
            return (await GetAllUsersAsync()).FirstOrDefault(u => u.UserName == userName);
        }

        public async Task<User> FindUserByFacilityAsync(string facilityId)
        {
            return (await GetAllUsersAsync()).FirstOrDefault(u => u.FacilityId == facilityId);
        }

        private async Task<IEnumerable<User>> GetAllUsersAsync()
        {
            using (var stream = new FileStream("Users.json", FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                var reader = new StreamReader(stream);

                var users = await Task.Factory.StartNew(() => JsonConvert.DeserializeObject<User[]>(reader.ReadToEnd()));

                return users;
            }
        }
    }
}
