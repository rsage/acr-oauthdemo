﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Infrastructure;

namespace Acr.AuthDemo.Core.OAuth
{
    public class AccessTokenProvider : IAuthenticationTokenProvider
    {
        private readonly ConcurrentDictionary<string, AuthenticationTicket> _tokens = new ConcurrentDictionary<string, AuthenticationTicket>();
        public void Create(AuthenticationTokenCreateContext context)
        {
            var guid = Guid.NewGuid().ToString();
            Trace.TraceInformation("New refresh token was issued: {0}", guid);
            var ticket = CreateTicket(context.Ticket);

            _tokens.TryAdd(guid, ticket);

            context.SetToken(guid);
        }

        public async Task CreateAsync(AuthenticationTokenCreateContext context)
        {
            Create(context);
        }

        public void Receive(AuthenticationTokenReceiveContext context)
        {
            AuthenticationTicket ticket;
            if (!_tokens.TryRemove(context.Token, out ticket))
            {
                Trace.TraceError("Provided refresh token {0} doesn't exist.", context.Token);
            };

            ticket = CreateTicket(ticket);

            Trace.TraceInformation("New access token was creaded by provided refresh token: {0}", context.Token);

            _tokens.TryAdd(context.Token, ticket);
            context.SetTicket(ticket);
        }

        private static AuthenticationTicket CreateTicket(AuthenticationTicket baseTicket)
        {
            var properties = new AuthenticationProperties(baseTicket.Properties.Dictionary)
            {
                IssuedUtc = baseTicket.Properties.IssuedUtc,
                ExpiresUtc = DateTime.UtcNow.AddMinutes(1)
            };

            var refreshTokenTicket = new AuthenticationTicket(baseTicket.Identity, properties);
            return refreshTokenTicket;
        }

        public async Task ReceiveAsync(AuthenticationTokenReceiveContext context)
        {
            Receive(context);
        }
    }
}