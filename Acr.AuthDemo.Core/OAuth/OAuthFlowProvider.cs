using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Security.Claims;
using System.Threading.Tasks;
using Acr.AuthDemo.Core.Data;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;

namespace Acr.AuthDemo.Core.OAuth
{
    public class OAuthFlowProvider : OAuthAuthorizationServerProvider
    {
        #region Fields

        private const string RoleClaimType = "http://schemas.microsoft.com/ws/2008/06/identity/claims/role";
        private const string UserIdClaimType = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier";
        private const string UserNameClaimType = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name";

        private readonly UsersRepository _usersRepository = new UsersRepository();

        #endregion Fields

        #region Auth Workflow

        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            try
            {
                Trace.TraceInformation("Client application authentication...");

                var grantType = context.Parameters.Get("grant_type");
                var scope = context.Parameters.Get("scope");

                Trace.TraceInformation("Grant type specifed: {0}.", grantType);
                Trace.TraceInformation("Scope specifed: {0}.", scope);

                if (grantType != "refresh_token")
                {
                    if (string.IsNullOrEmpty(scope))
                    {
                        Trace.TraceError("Client application authentication failed: no scope was specified.");
                        context.Rejected();
                        return;
                    }
                    if (scope != "nuance")
                    {
                        Trace.TraceError(
                            "Client application authentication failed: '{0}' scope was specified while 'nuance' was expected.",
                            scope);
                        context.Rejected();
                        return;
                    }
                }

                string facilityid;
                string pass;
                context.TryGetFormCredentials(out facilityid, out pass);

                Trace.TraceInformation("Client (facility) id specifed: {0}.", facilityid);

                if (string.IsNullOrEmpty(facilityid))
                {
                    Trace.TraceError("Client application authentication failed: no facility id provided.");
                    context.Rejected();
                    return;
                }

                var user = await _usersRepository.FindUserByFacilityAsync(facilityid);
                if (user == null)
                {
                    Trace.TraceError(
                        "Client application authentication failed: no registered user with facility id {0}.", facilityid);
                    context.Rejected();
                    return;
                }

                context.OwinContext.Set("as:client_id", facilityid);
                context.Validated();
                Trace.TraceInformation("Client application {0} was authenticated successfully to access '{1}' scope.",
                    facilityid, scope);
            }
            catch (Exception e)
            {
                Trace.TraceError(e.ToString());
                throw;
            }
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            try
            {
                Trace.TraceInformation("User authentication...");
                Trace.TraceInformation("User name: {0}.", context.UserName);

                var user = await _usersRepository.FindUserAsync(context.UserName);
                if (user == null)
                {
                    Trace.TraceError("User authentication failed: user name is unknown.");
                    context.Rejected();
                    return;
                }

                Trace.TraceInformation("Provided password: {0}.", context.Password);
                Trace.TraceInformation("Actual password: {0}.", user.Password);
                if (context.Password != user.Password)
                {
                    Trace.TraceError("User authentication failed: password is invalid.");
                    context.Rejected();
                    return;
                }

                var identity = CreateIdentity(context.UserName);
                var properties = CreateProperties(context);
                var ticket = new AuthenticationTicket(identity, properties);
                context.Validated(ticket);
                Trace.TraceInformation("User {0} was authenticated successfully.", context.UserName);
            }
            catch (Exception e)
            {
                Trace.TraceError(e.ToString());
                throw;
            }
        }

        public override async Task GrantRefreshToken(OAuthGrantRefreshTokenContext context)
        {
            try
            {
                Trace.TraceInformation("Refresh token validation...");

                var originalClient = context.Ticket.Properties.Dictionary["as:client_id"];
                var currentClient = context.OwinContext.Get<string>("as:client_id");

                if (originalClient != currentClient)
                {
                    Trace.TraceError("Client ID provided doesn't match the refresh token owner client id.");
                    context.Rejected();
                    return;
                }

                var id = new ClaimsIdentity(context.Ticket.Identity);
                var ticket = new AuthenticationTicket(id, context.Ticket.Properties);
                context.Validated(ticket);
                Trace.TraceInformation("New access token was granted.");
            }
            catch (Exception e)
            {
                Trace.TraceError(e.ToString());
                throw;
            }
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            try
            {
                foreach (var property in context.Properties.Dictionary)
                {
                    context.AdditionalResponseParameters.Add(property.Key, property.Value);
                }

                return Task.FromResult<object>(null);
            }
            catch (Exception e)
            {
                Trace.TraceError(e.ToString());
                throw;
            }
        }

        #endregion Auth Workflow

        #region Helpers

        private static AuthenticationProperties CreateProperties(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var data = new Dictionary<string, string>
            {
                {"userName", context.UserName},
                {"as:client_id", context.ClientId ?? "clientid"}
            };
            return new AuthenticationProperties(data);
        }

        private static ClaimsIdentity CreateIdentity(string userName)
        {
            var id = new ClaimsIdentity(OAuthDefaults.AuthenticationType, UserNameClaimType, RoleClaimType);
            id.AddClaim(new Claim(UserIdClaimType, userName, "http://www.w3.org/2001/XMLSchema#string"));
            id.AddClaim(new Claim(UserNameClaimType, userName, "http://www.w3.org/2001/XMLSchema#string"));
            id.AddClaim(new Claim("http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider", "TRIAD Demo", "http://www.w3.org/2001/XMLSchema#string"));
            id.AddClaim(new Claim(RoleClaimType, "ACR User", "http://www.w3.org/2001/XMLSchema#string"));
            return id;
        }

        #endregion Helpers
    }
}