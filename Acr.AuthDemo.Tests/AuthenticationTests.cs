﻿using System;
using System.Security.Authentication;
using System.Threading.Tasks;
using Acr.AuthDemo.ClientApi;
using Acr.AuthDemo.Tests.Properties;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Acr.AuthDemo.Tests
{
    [TestClass]
    public class AuthenticationWithValidClientIdTests
    {
        private DemoClient _target;

        [TestInitialize]
        public void SetUp()
        {
            _target = new DemoClient(new Uri(Settings.Default.ServiceBaseUrl, UriKind.Absolute), "123");
        }

        [TestCleanup]
        public void TearDown()
        {
            _target.Dispose();
            _target = null;
        }

        [TestMethod]
        public async Task AuthenticateTest()
        {
            await _target.AuthenticateAsync("user1", "12345");

            Assert.IsNotNull(_target.RefreshToken);
        }

        [TestMethod]
        [ExpectedException(typeof(AuthenticationException))]
        public async Task AuthenticateUsingInvalidPasswordTest()
        {
            await _target.AuthenticateAsync("user1", "invalid password");

            Assert.IsNotNull(_target.RefreshToken);
        }

        [TestMethod]
        [ExpectedException(typeof(AuthenticationException))]
        public async Task AuthenticateUsingInvalidUserNameTest()
        {
            await _target.AuthenticateAsync("invalid user", "12345");

            Assert.IsNotNull(_target.RefreshToken);
        }
    }
}
