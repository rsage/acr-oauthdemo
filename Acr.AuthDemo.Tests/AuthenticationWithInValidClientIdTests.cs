﻿using System;
using System.Security.Authentication;
using System.Threading.Tasks;
using Acr.AuthDemo.ClientApi;
using Acr.AuthDemo.Tests.Properties;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Acr.AuthDemo.Tests
{
    [TestClass]
    public class AuthenticationWithInValidClientIdTests
    {
        private DemoClient _target;

        [TestInitialize]
        public void SetUp()
        {
            _target = new DemoClient(new Uri(Settings.Default.ServiceBaseUrl, UriKind.Absolute), "invalid client");
        }

        private void TearDown()
        {
            _target.Dispose();
            _target = null;
        }

        [TestMethod]
        [ExpectedException(typeof(AuthenticationException))]
        public async Task AuthenticateWithInvalidClientIdTest()
        {
            await _target.AuthenticateAsync("user1", "12345");
        }
    }
}