using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Acr.AuthDemo.ClientApi;
using Acr.AuthDemo.Common;
using Acr.AuthDemo.Tests.Properties;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Acr.AuthDemo.Tests
{
    [TestClass]
    public class FileApiTests
    {
        private DemoClient _target;

        [TestInitialize]
        public void SetUp()
        {
            _target = new DemoClient(new Uri(Settings.Default.ServiceBaseUrl, UriKind.Absolute), "123");
            _target.AuthenticateAsync("user1", "12345").Wait();
        }

        private void TearDown()
        {
            _target.Dispose();
            _target = null;
        }

        [TestMethod]
        public async Task UploadFileTest()
        {
            using (var stream = new MemoryStream(Resources.File))
            {
                await _target.UploadFileAsync(stream, "1.vsd");
            }
        }

        [TestMethod]
        public async Task GetFileTest()
        {
            using (var stream = await _target.GetFileAsync("1.vsd"))
            {
                Assert.IsNotNull(stream);
            }
        }
    }
}