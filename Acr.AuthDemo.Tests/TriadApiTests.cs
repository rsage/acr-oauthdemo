﻿using System;
using System.Linq;
using System.Security.Authentication;
using System.Threading.Tasks;
using Acr.AuthDemo.ClientApi;
using Acr.AuthDemo.Common;
using Acr.AuthDemo.Tests.Properties;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Acr.AuthDemo.Tests
{
    [TestClass]
    public class TriadApiTests
    {
        private DemoClient _target;

        [TestInitialize]
        public void SetUp()
        {
            _target = new DemoClient(new Uri(Settings.Default.ServiceBaseUrl, UriKind.Absolute), "123");
            _target.AuthenticateAsync("user1", "12345").Wait();
        }

        private void TearDown()
        {
            _target.Dispose();
            _target = null;
        }

        [TestMethod]
        public async Task GetAllTrialsTest()
        {
            var actual = await _target.GetAllTrialsAsync();

            Assert.IsTrue(actual.Any());
        }

        [TestMethod]
        public async Task GetTrialTest()
        {
            var trials = await _target.GetAllTrialsAsync();
            var expectedId = trials.First().Id;

            var actual = await _target.GetTrialAsync(expectedId);

            Assert.AreEqual(expectedId, actual.Id);
        }

        [TestMethod]
        public async Task AddTrialTest()
        {
            var expectedId = Guid.NewGuid().ToString();
            var expectedTrial = new Trial
            {
                Id = expectedId,
                Name = "Trial" + expectedId,
                Projects = new[] { new Project { Name = "Project" + expectedId } }
            };

            await _target.AddTrialAsync(expectedTrial);

            var trials = await _target.GetAllTrialsAsync();
            Assert.IsNotNull(trials.Single(t => t.Id == expectedId));
        }

        [TestMethod]
        public async Task DeleteTrialTest()
        {
            var trials = await _target.GetAllTrialsAsync();
            var trialIdToDelete = trials.First().Id;

            await _target.DeleteTrialAsync(trialIdToDelete);

            trials = await _target.GetAllTrialsAsync();
            Assert.IsTrue(trials.All(t => t.Id != trialIdToDelete));
        }

    }
}
