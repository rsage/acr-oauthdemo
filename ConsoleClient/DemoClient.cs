﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Security.Authentication;
using System.Threading.Tasks;
using Thinktecture.IdentityModel.Client;

namespace ConsoleClient
{
    public class DemoClient: IDisposable
    {
        #region Fields

        private readonly string _facilityId;

        private readonly Uri _getTokenUrl;
        private readonly Uri _baseValueUrl;
        private readonly Uri _baseFilesUrl;
        private TokenResponse _authResponse;

        private readonly HttpClient _httpClient = new HttpClient();

        #endregion Fields

        #region Constructors

        public DemoClient(Uri baseApiUrl, string facilityId)
        {
            _facilityId = facilityId;

            var urlBuilder = new UriBuilder(baseApiUrl);

            urlBuilder.Path = "token";
            _getTokenUrl = urlBuilder.Uri;

            urlBuilder.Path = "api/values";
            _baseValueUrl = urlBuilder.Uri;

            urlBuilder.Path = "api/files";
            _baseFilesUrl = urlBuilder.Uri;
        }

        #endregion Constructors

        #region Public Methods

        #region Auth

        public async Task AuthenticateAsync(string userName, string password)
        {
            var authResponse = await OAuthClient.RequestResourceOwnerPasswordAsync(userName, password, "nuance");

            ValidateAuthError(authResponse);

            _authResponse = authResponse;
        }

        public async Task RefreshTokenAsync(string refreshToken)
        {

            var authResponse = await OAuthClient.RequestRefreshTokenAsync(refreshToken);

            ValidateAuthError(authResponse);

            _authResponse = authResponse;
        }

        public string RefreshToken
        {
            get
            {
                ValidateAuthorized();
                return _authResponse.RefreshToken;
            }
        }

        #endregion Auth

        #region API

        public async Task<string> GetValueAsync(int id)
        {
            await RefreshTokenIfRequiredAsync();

            var urlBuilder = new UriBuilder(_baseValueUrl);
            urlBuilder.Path = urlBuilder.Path +"/" + id;

            var result = await _httpClient.GetAsync(_baseValueUrl);

            return await result.Content.ReadAsStringAsync();
        }

        public async Task<Stream> GetFileAsync(int id)
        {
            await RefreshTokenIfRequiredAsync();

            var urlBuilder = new UriBuilder(_baseFilesUrl);
            urlBuilder.Path = urlBuilder.Path + "/" + id;

            return await _httpClient.GetStreamAsync(urlBuilder.Uri);
        }

        public async Task UploadFileAsync(Stream file)
        {
            await RefreshTokenIfRequiredAsync();

            HttpContent content = new StreamContent(file);

            await _httpClient.PostAsync(_baseFilesUrl, content);
        }

        #endregion API

        #endregion Public Methods

        #region Helpers
        private OAuth2Client OAuthClient
        {
            get { return new OAuth2Client(_getTokenUrl, _facilityId, null, OAuth2Client.ClientAuthenticationStyle.PostValues); }
        }

        private async Task RefreshTokenIfRequiredAsync()
        {
            ValidateAuthorized();

            if (_authResponse.Json.Value<DateTime>(".expires") <= DateTime.Now)
            {
                await RefreshTokenAsync(_authResponse.RefreshToken);
            }

            _httpClient.SetBearerToken(_authResponse.AccessToken);
        }

        private void ValidateAuthorized()
        {
            if (_authResponse == null)
            {
                throw new InvalidOperationException("Authentication information is absent. Authenticate before any other operation perfoming.");
            }
        }

        private static void ValidateAuthError(TokenResponse authResponse)
        {
            if (authResponse.IsError)
            {
                throw new AuthenticationException(authResponse.Error);
            }
        }

        #endregion Helpers

        #region IDisposable implementation

        public void Dispose()
        {
            if (_httpClient != null)
            {
                _httpClient.Dispose();
            }
        }

        #endregion IDisposable implementation
    }
}