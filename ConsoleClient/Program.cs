﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ConsoleClient
{
    static class Program
    {
        private static readonly Uri DefaultBaseUrl = new Uri("http://localhost:8888", UriKind.Absolute);

        private static void Main(string[] args)
        {
            using (var client = new DemoClient(GetServiceUrl(args), "123"))
            {
                client.AuthenticateAsync("user1", "12345").Wait();

                TestGetValue(client);

                TestGetFile(client);

                TestUploadFile(client);
            }
            Console.ReadKey();
        }

        private static Uri GetServiceUrl(IList<string> args)
        {
            Uri serviceUrl;
            if (args.Count > 0 && Uri.TryCreate(args[0], UriKind.Absolute, out serviceUrl))
            {
                return serviceUrl;
            }
            return DefaultBaseUrl;
        }

        /// <summary>
        /// POST http://localhost:36397/api/files/ - Upload a file
        /// </summary>
        /// <param name="client"></param>
        private static void TestUploadFile(DemoClient client)
        {
            const string textToSend = "qweqw eqw qasd asd asdasd asd asdasd asdasd asdasdasd";
            using (var data = new MemoryStream(Encoding.UTF8.GetBytes(textToSend)))
            {
                client.UploadFileAsync(data).Wait();

                Console.WriteLine("File was uploaded");
            }
        }

        /// <summary>
        /// GET http://localhost:36397/api/files/1 - Get file with Id = 1
        /// </summary>
        /// <param name="client"></param>
        private static void TestGetFile(DemoClient client)
        {
            using (var data = client.GetFileAsync(1).Result)
            {
                var reader = new StreamReader(data);
                var text = reader.ReadToEnd();
                Console.WriteLine("File content: {0}", text);
            }
        }

        /// <summary>
        ///  GET http://localhost:36397/api/values/5 - Get string value with Id = 5
        /// </summary>
        private static void TestGetValue(DemoClient client)
        {
            var value = client.GetValueAsync(5).Result;

            Console.WriteLine("Value(5) = {0}", value);
        }
    }
}
