﻿using System;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;
using WebApiTinyDemo.OAuth;

namespace WebApiTinyDemo
{
    public partial class Startup
    {
        private static void ConfigureAuth(IAppBuilder app)
        {
            var options = new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/token"),
                Provider = new OAuthFlowProvider(),
                RefreshTokenProvider = new AccessTokenProvider(),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(1),
                AllowInsecureHttp = true
            };

            app.UseOAuthBearerTokens(options);
        }
    }
}