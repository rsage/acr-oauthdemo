using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace WebApiTinyDemo.Controllers
{
    [Authorize]
    public class FilesController : ApiController
    {
        // GET api/files/5
        public HttpResponseMessage Get(int id)
        {
            const string data = "qwerwqerqwerqwerqwerqwedfasdfwer    qwe qwe qweq    we  qqeq    weq weq we  qwe qwe d";
            var stream = new MemoryStream(Encoding.UTF8.GetBytes(data));

            var result = new HttpResponseMessage(HttpStatusCode.OK) {Content = new StreamContent(stream)};
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

            return result;
        }

        // POST api/files
        public async Task<HttpResponseMessage> Post()
        {
            using (var requestStream = await Request.Content.ReadAsStreamAsync())
            {
                try
                {
                    using (Stream fileStream = File.Create(HttpContext.Current.Server.MapPath("~/" + "text.txt")))
                    {
                        await requestStream.CopyToAsync(fileStream);
                    }
                }
                catch (IOException)
                {
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);
                }
            }

            return new HttpResponseMessage {StatusCode = HttpStatusCode.Created};
        }
    }
}