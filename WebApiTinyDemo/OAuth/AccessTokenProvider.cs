﻿using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Infrastructure;

namespace WebApiTinyDemo.OAuth
{
    public class AccessTokenProvider : IAuthenticationTokenProvider
    {
        private readonly ConcurrentDictionary<string, AuthenticationTicket> _tokens = new ConcurrentDictionary<string, AuthenticationTicket>();
        public void Create(AuthenticationTokenCreateContext context)
        {
            var guid = Guid.NewGuid().ToString();

            var ticket = CreateTicket(context.Ticket);

            _tokens.TryAdd(guid, ticket);

            context.SetToken(guid);
        }

        public async Task CreateAsync(AuthenticationTokenCreateContext context)
        {
            Create(context);
        }

        public void Receive(AuthenticationTokenReceiveContext context)
        {
            AuthenticationTicket ticket;
            if (!_tokens.TryRemove(context.Token, out ticket)) return;

            ticket = CreateTicket(ticket);
            _tokens.TryAdd(context.Token, ticket);
            context.SetTicket(ticket);
        }

        private static AuthenticationTicket CreateTicket(AuthenticationTicket baseTicket)
        {
            var properties = new AuthenticationProperties(baseTicket.Properties.Dictionary)
            {
                IssuedUtc = baseTicket.Properties.IssuedUtc,
                ExpiresUtc = DateTime.UtcNow.AddMinutes(1)
            };

            var refreshTokenTicket = new AuthenticationTicket(baseTicket.Identity, properties);
            return refreshTokenTicket;
        }

        public async Task ReceiveAsync(AuthenticationTokenReceiveContext context)
        {
            Receive(context);
        }
    }
}