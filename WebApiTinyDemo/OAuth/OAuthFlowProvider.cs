using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;

namespace WebApiTinyDemo.OAuth
{
    public class OAuthFlowProvider : OAuthAuthorizationServerProvider
    {
        #region Fields

        private const string RoleClaimType = "http://schemas.microsoft.com/ws/2008/06/identity/claims/role";
        private const string UserIdClaimType = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier";
        private const string UserNameClaimType = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name";

        #endregion Fields

        #region Auth Workflow

        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
            return;
            string clientId;
            string clientSecret;

            context.TryGetBasicCredentials(out clientId, out clientSecret);

            if (clientId == clientSecret)
            {
                context.OwinContext.Set("as:client_id", clientId);
                context.Validated();
            }
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            if (context.UserName != context.Password)
            {
                context.Rejected();
                return;
            }

            var identity = CreateIdentity(context.UserName);
            var properties = CreateProperties(context);
            var ticket = new AuthenticationTicket(identity, properties);
            context.Validated(ticket);
        }

        public override async Task GrantRefreshToken(OAuthGrantRefreshTokenContext context)
        {
            var originalClient = context.Ticket.Properties.Dictionary["as:client_id"];
            var currentClient = context.OwinContext.Get<string>("as:client_id");

            //if (originalClient != currentClient)
            //{
            //    context.Rejected();
            //    return;
            //}

            var id = new ClaimsIdentity(context.Ticket.Identity);
            var ticket = new AuthenticationTicket(id, context.Ticket.Properties);
            context.Validated(ticket);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (var property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }

        #endregion Auth Workflow

        #region Helpers

        private static AuthenticationProperties CreateProperties(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var data = new Dictionary<string, string>
            {
                {"userName", context.UserName},
                {"as:client_id", context.ClientId ?? "clientid"}
            };
            return new AuthenticationProperties(data);
        }

        private static ClaimsIdentity CreateIdentity(string userName)
        {
            var id = new ClaimsIdentity(OAuthDefaults.AuthenticationType, UserNameClaimType, RoleClaimType);
            id.AddClaim(new Claim(UserIdClaimType, userName, "http://www.w3.org/2001/XMLSchema#string"));
            id.AddClaim(new Claim(UserNameClaimType, userName, "http://www.w3.org/2001/XMLSchema#string"));
            id.AddClaim(new Claim("http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider", "TRIAD Demo", "http://www.w3.org/2001/XMLSchema#string"));
            id.AddClaim(new Claim(RoleClaimType, "ACR User", "http://www.w3.org/2001/XMLSchema#string"));
            return id;
        }

        #endregion Helpers
    }
}