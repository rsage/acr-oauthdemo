﻿using Microsoft.Owin;
using Owin;
using WebApiTinyDemo;

[assembly: OwinStartup(typeof(Startup))]

namespace WebApiTinyDemo
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
